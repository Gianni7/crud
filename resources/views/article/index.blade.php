<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center mt-5">
                <h1>Tutti gli articoli</h1>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            @foreach ($articles as $article)
            <div class="col-12 col-md-3">
                    <div class="card">
                        <img src="{{Storage::url($article->img)}}" class="card-img-top" alt="{{$article->name}}">
                        <div class="card-body">
                        <h5 class="card-title">{{$article->name}}</h5>
                        <h5 class="card-title">{{$article->topic}}</h5>
                        <p class="card-text">{{$article->description}}</p>
                        <a href="#" class="btn btn-primary">Dettaglio</a>
                        </div>
                    </div>
            </div>
            @endforeach
        </div>
    </div>
</x-layout>