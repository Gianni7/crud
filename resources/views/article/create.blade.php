<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center mt-4">
                <h1>Crea l'articolo</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="card shadow mb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <form action="{{route('article.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="mb-3 mt-5">
                                        <label for="" class="form-label">Titolo</label>
                                        <input type="text" class="form-control" name="name" id="">
                                    </div>
                                    <div class="mb-3">
                                        <label for="" class="form-label">Argomento</label>
                                        <input type="text" class="form-control" name="topic" id="">
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Descrizione</label>
                                        <textarea class="form-control" name="description" id="" cols="20" rows="10"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Inserisci Immagine</label>
                                        <input type="file" name="img" class="form-control" id="">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary mb-5 w-100">Invia</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>        
</x-layout>