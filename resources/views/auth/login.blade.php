<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 text-center mt-4">
                <h1>Login</h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <div class="card shadow mb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <form action="{{route('login')}}" method="POST">
                                    @csrf
                                    <div class="mb-3 mt-5">
                                        <label for="" class="form-label">Nome Utente</label>
                                        <input type="text" class="form-control" name="name" id="">
                                    </div>
                                    <div class="mb-3">
                                        <label for="" class="form-label">Email</label>
                                        <input type="email" class="form-control" name="email" id="">
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Password</label>
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-primary mb-5 w-100">Invia</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div> 
</x-layout>